<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PROJECT DECODE</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="css/small-business.css" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131744827-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-131744827-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1995820977131583');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1995820977131583&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->


</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/"><img src="vendor/img/brand.png" width="32" height="32" alt=""/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"/>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="/about">About Us
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/search"><span style="color: #28a745">Free Code</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div class="container">

    <div id="intro-dev" class="row my-4">
        <div class="col-lg-7">
            <img class="img-fluid rounded" id="browse" height="700px" src="/vendor/img/two.png" alt="">
        </div>

        <div class="col-lg-5">
            <br/>
            <h4>About Us</h4>
            <p>
                We are the group do developer started developing software and learning different techniques. The android
                is the technology which excites us and in the process of learning, we come across some tools which help
                a lot so we thought to came up with a solution which can help other developers to make their research
                and learning path easy.
            </p>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-4 mb-4">
            <div class="card">
                <div class="card-header text-capitalize text-center">
                    Why Ads?
                </div>
                <div class="card-body ">
                    <li>
                        <strong>Ads</strong> - an appropriate option, to run and maintain these tools available.
                    </li>
                </div>
            </div>
        </div>

        <div class="col-md-4 mb-4">
            <div class="card h-100">
                <div class="card-header text-capitalize text-center">
                    Why Us?
                </div>
                <div class="card-body">
                    <li>
                        <strong>We</strong> - been there, when it comes to research feature and developing them
                    </li>
                </div>
            </div>
        </div>

        <div class="col-md-4 mb-4">
            <div class="card">
                <div class="card-header text-capitalize text-center">
                    Why SE?
                </div>
                <div class="card-body">
                    <li>
                        <strong>SE</strong> - an easy way to learn and find what you need.
                    </li>
                </div>
            </div>
        </div>
    </div>


    <!--    <div class="card my-4 text-center">-->
    <!--        <div class="card-body">-->
    <!--            <p class="m-0">RESERVED FOR ADS</p>-->
    <!--        </div>-->
    <!--    </div>-->
    <!---->
    <!--    <div class="row my-4 ">-->
    <!--        <div class="col-lg-12">-->
    <!--            <div class="card">-->
    <!--                <div class="card-header">-->
    <!--                    OUR STORY-->
    <!--                </div>-->
    <!--                <div class="card-body text-justify">-->
    <!--                    <p class="text-capitalize text-justify">-->
    <!--                        after struggling in learning an deresearching how to do we found-->
    <!--                        we can use reverse eng to find references from already app for features and how to use .-->
    <!--                        we collected set of tools and resource which can help in order to make for next app research-->
    <!--                        easy-->
    <!--                    </p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->


    <div class="card my-4 text-center">
        <div class="card-body">
            <p class="m-0">RESERVED FOR ADS</p>
        </div>
    </div>

    <div class="row my-4 ">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    OUR OFFERING
                </div>
                <div class="card-body text-justify">
                    <p class="text-capitalize text-justify">
                        after struggling in learning an deresearching how to do we found
                        we can use reverse eng to find references from already app for features and how to use .
                        we collected set of tools and resource which can help in order to make for next app research
                        easy
                    </p>
                    <ul>
                        <li><strong class="text-capitalize">reverser engineering</strong></li>
                        <li><strong class="text-capitalize">ui reference</strong><span
                                    class="text-info text-capitalize">&nbsp;comming soon</span></li>
                        <li><strong class="text-capitalize">android library</strong><span
                                    class="text-info text-capitalize">&nbsp;comming soon</span></li>
                        <li><strong class="text-capitalize">android full code</strong><span
                                    class="text-info text-capitalize">&nbsp;comming soon</span></li>
                        <li><strong class="text-capitalize">assects</strong><span class="text-info text-capitalize">&nbsp;comming soon</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="card my-4 text-center">
        <div class="card-body">
            <p class="m-0">RESERVED FOR ADS</p>
        </div>
    </div>

    <div class="row my-4 ">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    CONTACT US
                </div>
                <div class="card-body text-justify">
                    <ul>
                        <li><strong class="text-capitalize">For Request / Query</strong></li>
                        <p>Email Us at : Contact@apkdecomplie.com</p>

                        <li><strong class="text-capitalize">For DMCA / REPORT</strong><span</li>
                        <p>Email Us at : Report@apkdecomplie.com</p>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</body>


<!--     Footer-->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">ApkDecompile.com</p>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
</html>
