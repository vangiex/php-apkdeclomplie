<?php

/*
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\Process\Process;

$Worker_url = '';

// DataBase Config
function db_connect()
{
    // db connection params
    $servername = "localhost";
    $username = "nh6yaj";
    $password = "qwerty@1234";
    $dbname = "task_queue";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        // error log
        return $conn->connect_error;
    }

    // return conn object
    return $conn;
}

// excute db query
function query($sql)
{
    // get conn object
    $conn = db_connect();
    // execute query
    $result = $conn->query($sql);
    // close conn
    $conn->close();
    // return result
    return $result;
}

$app->get('/[/]', function ($request, $response, $args) {

    $new_task = 'disallow';
    $tool = 'both';
    $uid = '';
    $current_path = '';
    $file_name = '';
    setcookie('uid', $uid, 0, "/"); // 86400 = 1 day
    setcookie('current_path', $current_path, 0, "/"); // 86400 = 1 day
    setcookie('file_name', $file_name, 0, "/"); // 86400 = 1 day
    setcookie('new_task', $new_task, 0, "/"); // 86400 = 1 day
    setcookie('tool', $tool, 0, "/"); // 86400 = 1 day
    include 'template/index.php';
    exit();
});

$app->get('/apktool[/]', function ($request, $response, $args) {

    $new_task = 'disallow';
    $tool = 'apktool';
    $uid = '';
    $current_path = '';
    $file_name = '';
    setcookie('uid', $uid, 0, "/"); // 86400 = 1 day
    setcookie('current_path', $current_path, 0, "/"); // 86400 = 1 day
    setcookie('file_name', $file_name, 0, "/"); // 86400 = 1 day
    setcookie('new_task', $new_task, 0, "/"); // 86400 = 1 day
    setcookie('tool', $tool, 0, "/"); // 86400 = 1 day
    include 'template/index.php';
    exit();
});

$app->get('/jadx[/]', function ($request, $response, $args) {

    $new_task = 'disallow';
    $tool = 'jadx';
    $uid = '';
    $current_path = '';
    $file_name = '';
    setcookie('uid', $uid, 0, "/"); // 86400 = 1 day
    setcookie('current_path', $current_path, 0, "/"); // 86400 = 1 day
    setcookie('file_name', $file_name, 0, "/"); // 86400 = 1 day
    setcookie('new_task', $new_task, 0, "/"); // 86400 = 1 day
    setcookie('tool', $tool, 0, "/"); // 86400 = 1 day
    include 'template/index.php';
    exit();
});

$app->get('/download[/]', function ($request, $response, $args) {


    if (isset($_COOKIE['new_task'])) {
        $new_task = $_COOKIE['new_task'];
    } else {
        $new_task = '';
    }

    if ($new_task == 'disallow') {
        return $response->withRedirect('/');
    } else {
        include 'template/download.php';
    }
    exit();
});

$app->get('/progress[/]', function ($request, $response, $args) {

    if (isset($_COOKIE['new_task'])) {
        $new_task = $_COOKIE['new_task'];
    } else {
        $new_task = '';
    }

    if ($new_task == 'disallow') {
        return $response->withRedirect('/');
    } else {
        include 'template/progress.php';
    }
    exit();
});

$app->get('/about[/]', function ($request, $response, $args) {
    include 'template/about.php';
    exit();
});

$app->get('/search[/]', function ($request, $response, $args) {
    include 'template/search.php';
    exit();
});

$app->get('/server[/]', function ($request, $response, $args) {
    PHPinfo();
    exit();
});

$app->get('/queue/authcode/qwerty[/]', function ($request, $response, $args) {
    $sql = "SELECT * FROM `Worker_Queue` WHERE `STATUS`= 'false' AND `SYNCED` = 'false' ";

    $result = query($sql);
    $rows = [];

    if ($result->num_rows > 0) {
        $rows = array();
        while ($Row = $result->fetch_assoc()) {
            $rows[] = $Row;
        }

        return $response->withJson($rows,
            200,
            JSON_UNESCAPED_UNICODE);
    } else {
        return $response->withJson($rows,
            200,
            JSON_UNESCAPED_UNICODE);
    }

});

$app->post('/queue/authcode/qwerty/{uid}[/]', function ($request, $response, $args) {

    $respn = [
        'status' => '',
        'msg' => '',
        'action' => ''
    ];

    $file_name = $_FILES['file']['name'];
    $uid = $args['uid'];

    if (isset($file_name)) {
        if (0 < $_FILES['file']['error']) {
            echo 'Error during file upload' . $file_name;
        } else {

            if (file_exists('results/' . $file_name)) {
                echo 'File already exists : results/' . $file_name;
            } else {

                if (!file_exists(getcwd() . "/results")) {
                    mkdir(getcwd() . "/results", 0777, true);
                }

                $upload_status = move_uploaded_file($_FILES['file']['tmp_name'], getcwd() . "/results/" . $file_name);

                if ($upload_status) {
                    $sql = "UPDATE `Worker_Queue` SET `STATUS`= 'true' WHERE `UID`= '$uid'";

                    $result = query($sql);
                    if ($result === true) {

                    } else {
                        echo $upload_status;
                    }

                } else {

                    echo 'error while uploading ' . $file_name;
                }
            }
        }
    } else {
        echo 'Please choose a file';
    }

    exit();
});

$app->post('/synced/{uid}[/]', function ($request, $response, $args) {
    $uid = $args['uid'];
    $sql = "UPDATE `Worker_Queue` SET `SYNCED` = 'true' WHERE `UID`= '$uid'";

    $result = query($sql);
    if ($result === true) {

    } else {
        echo $result;
    }

    exit();
});

$app->post('/upload[/]', function ($request, $response, $args) {

    date_default_timezone_set('Asia/Kolkata');

    if (isset($_COOKIE['tool'])) {
        $tool = $_COOKIE['tool'];
    } else {
        $tool = '';
    }

    $new_task = 'allow';
    $uid = uniqid();

    $file_name = substr(str_replace(')', '-',
            str_replace('(', '-',
                str_replace(' ', '-',
                    $_FILES['file']['name']))), 0, -4) . '-' . $uid . ".apk";
    $file_loc = 'results/' . substr(str_replace(')', '-',
            str_replace('(', '-',
                str_replace(' ', '-',
                    $_FILES['file']['name']))), 0, -4) . '-' . $uid;
    $file_url = "https://" . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/uploads/' . $file_name;
    $email = "";
    $date = date("d-m-y");
    $time = date("h:i");
    $status = 'false';


    $respn = [
        'status' => '500',
        'msg' => '',
        'action' => '/progress'
    ];

    if (isset($file_name)) {
        if (0 < $_FILES['file']['error'] or 100000000 < $_FILES['file']['size'] or  strpos($_FILES['file']['name'], '.apk') == false ) {
            $respn['status'] = '500';
            $respn['msg'] = 'Invaild File Size ' . $_FILES['file']['name'];
        } else {
            if (file_exists('uploads/' . $file_name)) {
                $respn['status'] = '500';
                $respn['msg'] = 'Invaild File ' . $_FILES['file']['name'];
            } else {

                if (!file_exists(getcwd() . "/uploads")) {
                    mkdir(getcwd() . "/uploads", 0777, true);
                }

                $upload_status = move_uploaded_file($_FILES['file']['tmp_name'], getcwd() . "/uploads/" . $file_name);

                if ($upload_status) {
                    $sql = "INSERT INTO `Worker_Queue` (`UID`, `FILE_NAME`, `FILE_LOC`, `FILE_URL`, `TOOL`, `DATE`, `TIME`, `STATUS` , `SYNCED`) 
                              VALUES ('$uid', '$file_name', '$file_loc', '$file_url', '$tool', '$date', '$time', '$status' , 'false')";

                    setcookie('uid', $uid, 0, "/"); // 86400 = 1 day
                    setcookie('new_task', $new_task, 0, "/"); // 86400 = 1 day
                    setcookie('tool', $tool, 0, "/"); // 86400 = 1 day

                    $result = query($sql);
                    if ($result === true) {
                        $respn['status'] = '200';
                        $respn['msg'] = 'task assigned';
                    } else {
                        $respn['status'] = '500';
                        $respn['msg'] = 'task can not be assigned';
                    }


                } else {
                    $respn['status'] = '500';
                    $respn['msg'] = 'task can not be assigned';
                }
            }
        }
    } else {
        $respn['status'] = '500';
        $respn['msg'] = 'File Upload Error';
    }

    echo json_encode($respn);
    exit();
});

$app->post('/update/{uid}[/]', function ($request, $response, $args) {

    $uid = $args['uid'];
    $current_path = '';
    $file_name = '';

    $respn = [
        'status' => '500',
        'msg' => 'task under process',
        'action' => '/download'
    ];

    $sql = "SELECT * FROM `Worker_Queue` WHERE `STATUS`= 'true' AND `UID` = '$uid' ";
    $result = query($sql);
    if ($result->num_rows > 0) {
        $result = $result->fetch_array();
        $zip = new ZipArchive;
        $res = $zip->open($result['FILE_LOC'] . '.zip');
        if ($res === TRUE) {
            $zip->extractTo($result['FILE_LOC']);
            $zip->close();
            $current_path = $result['FILE_LOC'];
            $file_name = $result['FILE_LOC'];
            $respn['status'] = '200';
            $respn['msg'] = 'task done';
            setcookie('current_path', $current_path, 0, "/"); // 86400 = 1 day
            setcookie('file_name', $file_name, 0, "/"); // 86400 = 1 day
            echo json_encode($respn);
        } else {
            $respn['status'] = '404';
            $respn['msg'] = 'Unable To Decode' . $result['FILE_LOC'] . '.zip';
            echo json_encode($respn);
        }
    } else {
        echo json_encode($respn);
    }
    exit();
});



